# DBC BusApp

Project developed by [Patrick Bard](https://www.linkedin.com/in/patrickbard/) as a test for DBC Company.

### TODO
* Improve test coverage
* Create algorithm to disambiguate street names


### Disclaimer

This project was generated with [ngX-Rocket](https://github.com/ngx-rocket/generator-ngx-rocket/)
version 5.3.0

#### Using

- [Angular](https://angular.io)
- [Bootstrap 4](https://getbootstrap.com)
- [ng-bootsrap](https://ng-bootstrap.github.io/)
- [Font Awesome](http://fontawesome.io)
- [RxJS](http://reactivex.io/rxjs)
- [ngx-translate](https://github.com/ngx-translate/core)
- [Lodash](https://lodash.com)
