import {Component, OnInit} from '@angular/core';
import {catchError, finalize, map} from 'rxjs/operators';
import {BusLine} from '@app/home/model/bus-line.model';
import {MicroBusLineModel} from '@app/home/model/microbus-line.model';
import {forkJoin, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {ItineraryButtonComponent} from '@app/home/itinerary-button/itinerary-button.component';
import {TranslateService} from '@ngx-translate/core';
import {DataPoaApiService} from '@app/api/data-poa-api.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public columnDefs = [
        {
            headerName: 'home.grid.code',
            headerValueGetter: this.translateHeaderValue.bind(this),
            field: 'code',
            sortable: true,
            filter: true
        },
        {
            headerName: 'home.grid.name',
            headerValueGetter: this.translateHeaderValue.bind(this),
            field: 'name',
            sortable: true,
            filter: true
        },
        {
            headerName: 'home.grid.type',
            headerValueGetter: this.translateHeaderValue.bind(this),
            valueGetter: this.translateCellValue.bind(this),
            field: 'type',
            sortable: true,
            filter: true
        },
        {
            headerName: 'home.grid.actions',
            headerValueGetter: this.translateHeaderValue.bind(this),
            field: '',
            cellRendererFramework: ItineraryButtonComponent,
            pinned: 'right'
        }
    ];
    public gridOptions = {
        localeTextFunc: this.translateGridOptions.bind(this)
    };
    public rowData: Observable<any[]>;
    public isLoading: boolean;
    public error: boolean;
    private gridApi: any;

    constructor(private router: Router,
                private dataPoa: DataPoaApiService,
                private translate: TranslateService) {
    }

    translateGridOptions(key: string, defaultValue: string) {
        let gridKey = `grid.${key}`;
        let value = this.translate.instant(gridKey);

        return (value === gridKey) ? defaultValue : value;
    }

    translateHeaderValue(params: any) {
        return this.translate.instant(params.colDef.headerName);
    }

    translateCellValue(params: any) {
        return this.translate.instant(params.data[params.colDef.field]);
    }

    getData() {
        const busData = this.dataPoa.getBusLines()
            .pipe(
                map((data: any[]) =>
                    data.map((item: any) => new BusLine(item.id, item.codigo, item.nome)))
            );
        const microBusData = this.dataPoa.getMicroBusLines()
            .pipe(
                map((data: any[]) =>
                    data.map((item: any) => new MicroBusLineModel(item.id, item.codigo, item.nome)))
            );

        return forkJoin([busData, microBusData])
            .pipe(
                map(responses => {
                    return [].concat(...responses);
                }),
                finalize(() => {
                    this.gridApi.sizeColumnsToFit();
                    this.isLoading = false;
                })
            );
    }

    ngOnInit() {
        this.isLoading = true;
        this.rowData = this.getData();

        this.translate.onLangChange.subscribe(() => {
            this.gridApi.refreshHeader();
            this.gridApi.refreshCells();
        });
    }

    onGridReady(params: any) {
        this.gridApi = params.api;
    }
}
