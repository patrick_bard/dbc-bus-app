import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {Angulartics2Module} from 'angulartics2';

import {CoreModule} from '@app/core';
import {SharedModule} from '@app/shared';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {AgGridModule} from 'ag-grid-angular';
import {ItineraryButtonComponent} from '@app/home/itinerary-button/itinerary-button.component';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        CoreModule,
        SharedModule,
        Angulartics2Module,
        HomeRoutingModule,
        AgGridModule.withComponents([ItineraryButtonComponent])
    ],
    declarations: [
        HomeComponent
    ],
    providers: []
})
export class HomeModule {
}
