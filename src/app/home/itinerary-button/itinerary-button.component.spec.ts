import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ItineraryButtonComponent} from './itinerary-button.component';
import {SharedModule} from '@app/shared';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Angulartics2Module} from 'angulartics2';
import {RouterTestingModule} from '@angular/router/testing';
import {CoreModule} from '@app/core';
import {By} from '@angular/platform-browser';

describe('ItineraryButtonComponent', () => {
    let component: ItineraryButtonComponent;
    let fixture: ComponentFixture<ItineraryButtonComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                Angulartics2Module.forRoot([]),
                CoreModule,
                SharedModule,
                HttpClientTestingModule,
            ],
            declarations: [ItineraryButtonComponent]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ItineraryButtonComponent);
        component = fixture.componentInstance;
        component.agInit({
            data: {id: '123'}
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create a link to itinerary', function () {
        // given
        let href = fixture.debugElement.query(By.css('a')).nativeElement.getAttribute('href');

        // when

        // then
        expect(href).toEqual('/itinerary/123');
    });
});
