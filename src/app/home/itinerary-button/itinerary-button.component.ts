import {Component} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';

@Component({
    selector: 'itinerary-link',
    template: `<a [routerLink]="['/itinerary', params.data.id]" class="fa fa-search action-icon"></a>`,
})
export class ItineraryButtonComponent implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    refresh(): boolean {
        return false;
    }
}
