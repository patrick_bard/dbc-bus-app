import {TransportLine} from './transport-line.model';

export class MicroBusLineModel extends TransportLine {
    constructor(public id: number,
                public code: string,
                public name: string) {
        super(id, code, name, 'Micro Bus');
    }
}
