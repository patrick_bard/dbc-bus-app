import {TransportLine} from './transport-line.model';

export class BusLine extends TransportLine {
    constructor(public id: number,
                public code: string,
                public name: string) {
        super(id, code, name, 'Bus');
    }
}
