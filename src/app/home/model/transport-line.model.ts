export class TransportLine {
    constructor(
        public id: number,
        public code: string,
        public name: string,
        public type: string,
    ) {
    }
}
