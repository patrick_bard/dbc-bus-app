import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ItineraryComponent} from '@app/itinerary/itinerary.component';
import {Shell} from '@app/shell/shell.service';

const routes: Routes = [
    Shell.childRoutes([
        {path: 'itinerary/:id', component: ItineraryComponent},
    ])
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class ItineraryRoutingModule {
}
