export interface ItineraryModel {
    idlinha: string;
    nome: string;
    codigo: string;

    [key: string]: any;
}
