import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ItineraryComponent} from './itinerary.component';
import {TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '@app/shared';
import {Angulartics2Module} from 'angulartics2';
import {CoreModule} from '@app/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('ItineraryComponent', () => {
    let component: ItineraryComponent;
    let fixture: ComponentFixture<ItineraryComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                Angulartics2Module.forRoot([]),
                TranslateModule.forRoot(),
                CoreModule,
                SharedModule,
                HttpClientTestingModule,
            ],
            declarations: [ItineraryComponent]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ItineraryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
