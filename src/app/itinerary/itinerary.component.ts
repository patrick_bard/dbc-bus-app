import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';
import {ItineraryModel} from '@app/itinerary/itinerary.model';
import {DataPoaApiService} from '@app/api/data-poa-api.service';
import {HereApiService} from '@app/api/here-api.service';

@Component({
    selector: 'app-itinerary',
    templateUrl: './itinerary.component.html',
    styleUrls: ['./itinerary.component.scss']
})
export class ItineraryComponent implements OnInit {
    public id: any;
    public isLoading: boolean;
    public data: Observable<ItineraryModel>;
    public lineRoute: any[];

    constructor(private route: ActivatedRoute,
                private dataPoa: DataPoaApiService,
                private here: HereApiService) {
    }

    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('id');
        this.isLoading = true;
        this.lineRoute = [];
        this.data = this.dataPoa.getLine(this.id);

        this.data.subscribe((value: ItineraryModel) => {
                let calls: any = [];

                Object.entries(value).forEach((entry: object) => {
                    if (!isNaN(entry[0])) {
                        let coordinates = entry[1];
                        calls.push(this.here.reverseGeocoder(coordinates));
                        this.lineRoute.push(coordinates);
                    }
                });

                forkJoin(...calls).subscribe(
                    data => { // Note: data is an array now
                        data.forEach((value: any, key: number) => {
                            if (value.Response.View[0] != null && value.Response.View[0].Result.length > 0) {
                                let results = value.Response.View[0].Result;

                                for (let j = 0; j < results.length; j++) {
                                    if (results[j].Location.Address.Street) {
                                        this.lineRoute[key].name = results[j].Location.Address.Street;
                                        break;
                                    }
                                }
                            }
                        });
                    }, err => console.log('error ' + err),
                    () => {
                        const result = [];
                        for (let i = 0; i < this.lineRoute.length; i++) {
                            if (i == 0 || (i > 0 && this.lineRoute[i].name != this.lineRoute[i - 1].name)) {
                                result.push(this.lineRoute[i]);
                            }
                        }
                        this.lineRoute = result;
                        this.isLoading = false;
                    }
                );
            },
            (err) => console.error(err),
            () => console.log('data observable complete')
        );
    }
}
