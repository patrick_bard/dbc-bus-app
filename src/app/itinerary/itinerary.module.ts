import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {Angulartics2Module} from 'angulartics2';

import {CoreModule} from '@app/core';
import {SharedModule} from '@app/shared';
import {ItineraryComponent} from '@app/itinerary/itinerary.component';
import {ItineraryRoutingModule} from '@app/itinerary/itinerary-routing.module';
import {AgGridModule} from 'ag-grid-angular';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        CoreModule,
        SharedModule,
        ItineraryRoutingModule,
        AgGridModule,
        Angulartics2Module,
    ],
    declarations: [
        ItineraryComponent
    ],
    providers: []
})
export class ItineraryModule {
}
