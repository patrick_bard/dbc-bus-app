import { TestBed } from '@angular/core/testing';

import { HereApiService } from './here-api.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('HereApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: HereApiService = TestBed.get(HereApiService);
    expect(service).toBeTruthy();
  });
});
