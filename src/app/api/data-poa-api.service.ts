import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ItineraryModel} from '@app/itinerary/itinerary.model';

@Injectable({
    providedIn: 'root'
})
export class DataPoaApiService {
    private baseUrl = 'http://www.poatransporte.com.br/php/facades/process.php';

    constructor(private http: HttpClient) {}

    getBusLines() {
        return this.http.get(`${this.baseUrl}?a=nc&t=o`);
    }

    getMicroBusLines() {
        return this.http.get(`${this.baseUrl}?a=nc&t=l`);
    }

    getLine(id: number) {
        return this.http.get<ItineraryModel>(`${this.baseUrl}?a=il&p=${id}`);
    }
}
