import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@env/environment.prod';
import {Coordinates} from '@app/itinerary/coordinates';

@Injectable({
    providedIn: 'root'
})
export class HereApiService {
    private appId = environment.api.here.appId;
    private appCode = environment.api.here.appCode;
    private server = 'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json';
    private readonly baseUrl: string;

    constructor(private http: HttpClient) {
        this.baseUrl = `${this.server}?app_id=${this.appId}&app_code=${this.appCode}`;
    }

    reverseGeocoder(coordinates: Coordinates) {
        return this.http.get(`${this.baseUrl}&prox=${coordinates.lat},${coordinates.lng}&mode=retrieveAddresses`);
    };
}
