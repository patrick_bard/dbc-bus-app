import {TestBed} from '@angular/core/testing';

import {DataPoaApiService} from './data-poa-api.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('DataPoaApiService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
    }));

    it('should be created', () => {
        const service: DataPoaApiService = TestBed.get(DataPoaApiService);
        expect(service).toBeTruthy();
    });
});
